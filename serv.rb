require "sinatra"

=begin
Get net info
=end
class Serv < Sinatra::Base
  get '/i' do
    "#{settings.store.get_net_info}"
  end
end
    
class NetInfo
    
  def tick
    @net_info = ""
    IO.popen("cat /proc/net/dev") do | stdout |
      while line = stdout.gets
        @net_info += line
      end
    end
  end
    
  def get_net_info() 
    return @net_info
  end
end
    
def init
  store = NetInfo.new
  Thread.new do
    while true do
      store.tick()
      sleep(1)
    end
  end

  Serv::set( :store, store )
  Serv::run!
end
    
init